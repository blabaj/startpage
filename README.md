# startpage

A simple startpage for everyday use. Features autocomplete for URLs, search history and custom autocompletes. Use `/help` for a list of commands.

The default background picture was taken by [Stephen Seeber](https://www.pexels.com/@stywo) and is licensed under the Pexel License
